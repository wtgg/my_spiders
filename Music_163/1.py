import wx
from loguru import logger


class App(wx.App):

    width = 600
    height = 600
    def OnInit(self):
        sr = self.get_screen_resolution()
        sr_x = sr['sr_x']
        sr_y = sr['sr_y']
        pos_x = int((sr_x - self.width)/2)
        pos_y = int((sr_y - self.height)/2)
        frame = wx.Frame(
            parent=None,
            title='这是窗体标题',
            pos=(pos_x, pos_y),  # 窗口置于屏幕中间
            size=(self.width, self.height)
        )  # 创建窗口
        frame.Show(show=True)  # 显示窗口
        return True

    def get_screen_resolution(self):
        width, height = wx.DisplaySize()
        return dict(sr_x=width, sr_y=height)


class Test(App):

    def run(self):
        mm = wx.DisplaySize()
        logger.debug(mm)


if __name__ == "__main__":
    app = App()  # 创建App类的实例
    app.MainLoop()  # 调用App类的MainLoop()主循环方法
    # t = Test()
    # screen_resolution = t.get_screen_resolution()
    # print(screen_resolution, type(screen_resolution))
