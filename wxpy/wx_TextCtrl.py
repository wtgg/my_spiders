import wx


class MyFrame(wx.Frame):

    def __init__(self, parent, id):
        wx.Frame.__init__(
            self,
            parent,
            id,
            title='创建TextCtrl类',
            pos=(100, 100),
            size=(400, 300)
        )

        panel = wx.Panel(self)  # 创建画板
        # 创建标题,并设置字体
        self.title = wx.StaticText(
            panel,
            label='请输入用户名和密码',
            pos=(140, 20)
        )
        # 文本和输入框
        self.label_user = wx.StaticText(panel, label='用户名:', pos=(50, 50))
        self.text_user = wx.TextCtrl(panel, pos=(100, 50), size=(235, 25), style=wx.TE_LEFT)

        self.label_pwd = wx.StaticText(panel, pos=(50, 90), label='密 码:')
        self.text_password = wx.TextCtrl(panel, pos=(100, 90), size=(235, 25), style=wx.TE_PASSWORD)

        # 按钮
        self.btn_confirm = wx.Button(panel, label='确定', pos=(105, 130))
        self.btn_cancel = wx.Button(panel, label='取消', pos=(195, 130))




if __name__ == "__main__":
    app = wx.App()
    frame = MyFrame(parent=None, id=-1)
    frame.Show()
    app.MainLoop()
